﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WeatherTest.Attributes
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
            bool userIsAuthenticated = filterContext.HttpContext.User.Identity.IsAuthenticated;
            if (!userIsAuthenticated)
            {
                filterContext.Controller.TempData["FalseAuthenticatedMessage"] =
                    "Вы запросили ресурс для доступа к которому необходима авторизация.";
            }
            else
            {
                if (filterContext.Controller.TempData != null)
                {
                    filterContext.Controller.TempData.Remove("FalseAuthenticatedMessage");
                }
            }
        }
    }
}