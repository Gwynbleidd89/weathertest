﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WeatherTest.Models;

namespace WeatherTest.Controllers
{
    public class TempController : Controller
    {

        private const string APIKEY1 = "58c2500edd1b4308aa4bf5063a7fcb03";
        private const string APIKEY2 = "6e2db436e22f45abb2481912182509";

        // GET: /Temp/Metcast/City
        [HttpGet]
        public async Task<ActionResult> Metcast(Cities.List city = Cities.List.Moscow)
        {
            if (!Enum.IsDefined(typeof(Cities.List), city))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            string cityString = city.ToString();

            string uri1 = $"https://api.weatherbit.io/v2.0/current?city={cityString}&key={APIKEY1}";
            string uri2 = $"http://api.worldweatheronline.com/premium/v1/weather.ashx?key={APIKEY2}&q={cityString}&tp=1&format=json"; // Более точный

            // Получаем результат запроса из WeatherBit
            string jsonString = await GetJsonStringAsync(uri1);

            MetCastViewModel metCastViewModel = new MetCastViewModel();

            // Если полученный json равен null, то запрашиваем прогноз из другого сервиса
            if (!String.IsNullOrEmpty(jsonString))
            {
                dynamic json = JsonConvert.DeserializeObject(jsonString);

                metCastViewModel = new MetCastViewModel
                {
                    City = city,
                    Temp = (Double)json.data[0].temp,
                    AppTemp = (Double)json.data[0].app_temp,
                    Pres = (Double)json.data[0].pres,
                    WindSpeed = (Double)json.data[0].wind_spd
                };
            }
            else
            {
                // Получаем резульат запроса из worldweatheronline
                jsonString = await GetJsonStringAsync(uri2);

                // Если полученный json равен null возвращаем BadGateway
                if (String.IsNullOrEmpty(jsonString))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadGateway);
                }

                dynamic json = JsonConvert.DeserializeObject(jsonString);

                // Констаты для рассчетов
                const UInt16 METERS_PER_KILOMETER = 1000;
                const UInt16 SECONDS_PER_HOUR = 3600;
                const UInt16 DIGITS_COUNT_AFTER_DECIMAL = 2;

                metCastViewModel = new MetCastViewModel
                {
                    City = city,
                    Temp = (Double)json.data.current_condition[0].temp_C,
                    AppTemp = (Double)json.data.current_condition[0].FeelsLikeC,
                    Pres = (Double)json.data.current_condition[0].pressure,
                    WindSpeed = Math.Round((Double)
                            json.data.current_condition[0].windspeedKmph * METERS_PER_KILOMETER / SECONDS_PER_HOUR
                            , DIGITS_COUNT_AFTER_DECIMAL
                        )
                };
            }

            return View(metCastViewModel);
        }

        /// <summary>
        /// Возвращает JsonString из Get запроса, заданного параметром uri, при ошибке вернется null.
        /// </summary>
        /// <param name="uri">Строка запроса.</param>
        /// <returns>Json результат, если запрос успешен, иначе null.</returns>
        public async Task<string> GetJsonStringAsync(string uri)
        {
            // Попытка получить json
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

                using (HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    return await reader.ReadToEndAsync();
                }
            }
            // При исключении вернется null
            catch
            {
                return null;
            }
        }

        // GET: /Temp/Capitals
        [HttpGet]
        public ActionResult Capitals()
        {
            return View();
        }
    }

}