﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WeatherTest.SMTP;

namespace WeatherTest
{
    public class MvcApplication : System.Web.HttpApplication
    {

        private const UInt32 FIFTEEN = 900000;

        // Создаем экзмпеляр службы рассылки прогнозов
        /// <summary>
        /// Экземпляр службы рассылки прогнозов.
        /// </summary>
        private MetcastEmailingService MES = new MetcastEmailingService();

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // Запускам службу, если она не запущена
            if (!MES.Status)
            {
                MES.StartService(FIFTEEN, FIFTEEN);
            }
        }

        protected void Application_Disposed()
        {
            // Останавливаем и очищаем ресурсы службы
            MES.Dispose();
        }
    }
}
