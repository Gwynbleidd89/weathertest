﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Runtime;
using System.Web.Mvc.Html;

namespace WeatherTest.Helpers
{
    public static class EnumHelper
    {
        /// <summary>
        /// Возвращает Html-элемент label и свойство Name атрибута <see cref="DisplayAttribute"/> для перечисления, представленного выражением.
        /// </summary>
        /// <typeparam name="TModel">Тип модели.</typeparam>
        /// <typeparam name="TEnum">Перечисление.</typeparam>
        /// <param name="html">Экземпляр вспомогательного метода HTML, который расширяется данным методом.</param>
        /// <param name="deleg">Выражение, которое определяет свойство для отображения.</param>
        /// <param name="htmlAttributes">Объект, содержащий атрибуты HTML, которые следует задать для элемента.</param>
        /// <returns>HTML-элемент label и свойство Name атрибута <see cref="DisplayAttribute"/> для перечисления, представленного выражением.</returns>
        /// <exception cref="ArgumentNullException">Возникает, если expression вернул null.</exception>
        /// <exception cref="ArgumentException">Возникает, если TEnum не является базовым типом перечеслений "Enum".</exception>
        public static MvcHtmlString EnumDisplayLabelFor<TModel, TEnum>
            (this HtmlHelper<TModel> html, Expression<Func<TModel, TEnum>> expression, object htmlAttributes = null)
        {
            if (expression == null)
            {
                throw new ArgumentNullException("expression");
            }

            if (!typeof(TEnum).IsEnum)
            {
                throw new ArgumentException("TEnum");
            }

            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            
            // Получаем имя свойства
            string propertyName = ExpressionHelper.GetExpressionText(expression);
            
            // Получаем значение свойства
            object propertyValue = metadata.Model;

            string displayName = propertyValue.GetType()
                .GetMember(propertyValue.ToString())
                .SingleOrDefault()
                .GetCustomAttribute<DisplayAttribute>()
                .GetName();

            if (String.IsNullOrEmpty(displayName))
            {
                displayName = propertyValue.ToString();
            }

            // Создаем Html тег "label".
            TagBuilder label = new TagBuilder("label");

            // Устанавливаем DisplayName
            label.InnerHtml = displayName;

            // Добавляем Html атрибуты
            label.Attributes.Add("id", propertyName);
            label.Attributes.Add("class", propertyName);
            label.Attributes.Add("style", "font-weight: normal");
            label.MergeAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));

            return MvcHtmlString.Create(label.ToString());
        }

        /// <summary>
        /// Возвращает Html-элемент "a" и свойство Name атрибута <see cref="DisplayAttribute"/> как текст ссылки, представленного перечислением.
        /// </summary>
        /// <param name="html">Экземпляр вспомогательного метода HTML, который расширяется данным методом.</param>
        /// <param name="linkText">Перечисление значение которого будет использовано как внутренний текст элемента привязки.</param>
        /// <param name="actionName">Имя действия.</param>
        /// <param name="controllerName">Имя контроллера.</param>
        /// <param name="routeValues">
        ///     Объект, содержащий параметры для маршрута. Параметры извлекаются через отражение
        ///     путем проверки свойств объекта. Объект обычно создается с использованием синтаксиса
        ///     инициализатора объектов.
        /// </param>
        /// <param name="htmlAttributes">Объект, содержащий атрибуты HTML, которые следует задать для элемента.</param>
        /// <returns>HTML-элемент "a" и свойство Name атрибута <see cref="DisplayAttribute"/> как текст ссылки, представленного перечислением.</returns>
        /// <exception cref="ArgumentNullException">Возникает, если linkText вернул null.</exception>
        public static MvcHtmlString EnumActionLink 
            (this HtmlHelper html, Enum linkText, string actionName, string controllerName, object routeValues = null, object htmlAttributes = null)
        {
            if (linkText == null)
            {
                throw new ArgumentNullException("linkText");
            }

            string displayName = linkText.GetType()
                .GetMember(linkText.ToString())
                .SingleOrDefault()
                .GetCustomAttribute<DisplayAttribute>()
                .GetName();

            if (String.IsNullOrEmpty(displayName))
            {
                displayName = linkText.ToString();
            }

            return html.ActionLink(displayName, actionName, controllerName, routeValues, htmlAttributes);
        }
    }
}