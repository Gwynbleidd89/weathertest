﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WeatherTest.Models;

namespace WeatherTest.Models
{

    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext()
            : base(nameOrConnectionString: "DefaultConnection")
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
    }
}