﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WeatherTest.Models
{
    public class Cities
    {
        /// <summary>
        /// Перечисление городов
        /// </summary>
        public enum List
        {
            [Display(Name = "Москва")]
            Moscow,
            [Display(Name = "Берлин")]
            Berlin,
            [Display(Name = "Лондон")]
            London,
            [Display(Name = "Париж")]
            Paris,
            [Display(Name = "Токио")]
            Tokio,
            [Display(Name = "Сингапур")]
            Singapore,
            [Display(Name = "Киев")]
            Kiev,
            [Display(Name = "Вашингтон")]
            Washington
        }
    }

    public class MetCastViewModel
    {
        public Cities.List City { get; set; }

        /// <summary>
        /// Задает или возвращает температуру в градусах Цельсия.
        /// </summary>
        [Display(Name = "Температура, градус Цельсия:")]
        public double Temp { get; set; }

        /// <summary>
        /// Задает или возвращает явную температуру в градусах Цельсия.
        /// </summary>
        [Display(Name= "Ощущается как, градус Цельсия:")]
        public double AppTemp { get; set; }

        /// <summary>
        /// Задает или возвращает давление в Мбар.
        /// </summary>
        [Display(Name = "Давление, Мбар:")]
        public double Pres { get; set; }

        /// <summary>
        /// Задает или вовзращает скорость ветра в метрах в секунду.
        /// </summary>
        [Display(Name = "Скорость ветра, Метр/с:")]
        public double WindSpeed { get; set; }
    }
}