﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using WeatherTest.Models;

namespace WeatherTest.SMTP
{
 
    /// <summary>
    /// Служба рассылки температуры адресатам.
    /// </summary>
    public class MetcastEmailingService : IDisposable
    {
        #region LifeCycle
        public MetcastEmailingService()
        {
            _timerCB = new TimerCallback(CheckAndEmailingAsync);
        }

        public void Dispose()
        {
            _semaphoreSlim.Dispose();
            db.Dispose();
            if (_timer != null)
            {
                _timer.Change(Timeout.Infinite, Timeout.Infinite);
                _timer.Dispose();
            }
        }
        #endregion

        #region Constants
        private const string APIKEY1 = "58c2500edd1b4308aa4bf5063a7fcb03";
        private const string APIKEY2 = "6e2db436e22f45abb2481912182509";
        #endregion

        #region Classes
        /// <summary>
        /// Адресат.
        /// </summary>
        private class Recipient
        {
            /// <summary>
            /// Задает или возвращает электронный адрес пользователя.
            /// </summary>
            [EmailAddress]
            public string Email { get; set; }

            /// <summary>
            /// Задает или возвращает предыдущую температуру в городе пользователя.
            /// </summary>
            public Double Temp { get; set; }

            /// <summary>
            /// Задает или возвращает город пользователя.
            /// </summary>
            public Cities.List City { get; set; }
        }
        
        /// <summary>
        /// Температура в определенном городе.
        /// </summary>
        private class TempOfCity
        {
            /// <summary>
            /// Задает или возвращает город.
            /// </summary>
            public Cities.List City { get; set; }
            /// <summary>
            /// Задает или возвращает температуру города.
            /// </summary>
            public Double Temp { get; set; }
        }
        #endregion

        #region Fields
        private ApplicationDbContext db = new ApplicationDbContext();

        /// <summary>
        /// Ставит накладывающиеся потоки в очередь.
        /// </summary>
        private SemaphoreSlim _semaphoreSlim = new SemaphoreSlim(1);

        /// <summary>
        /// Метод обратного вызова.
        /// </summary>
        private TimerCallback _timerCB;

        /// <summary>
        /// Таймер пула потоков данной службы.
        /// </summary>
        private System.Threading.Timer _timer;

        private bool _status;
        #endregion

        #region Properties
        /// <summary>
        /// Возвращает статус работы службы.
        /// </summary>
        public bool Status { get => _status; }
        #endregion

        #region Control
        /// <summary>
        /// Запускает службу.
        /// </summary>
        /// <param name="Period">Интервал в миллисекундах.</param>
        /// <param name="WaitUntilStart">Время ожидания до первой итерации в миллисекундах.</param>
        public void StartService(uint WaitUntilStart, uint Period)
        {
            if (_timer != null)
            {
                _timer.Change(Timeout.Infinite, Timeout.Infinite);
                _timer = null;
            }
            _timer = new Timer(_timerCB, null, WaitUntilStart, Period);
            _status = true;
        }

        /// <summary>
        /// Останавливает службу.
        /// </summary>
        public void StopService()
        {
            if (_timer != null)
            {
                _timer.Change(Timeout.Infinite, Timeout.Infinite);
            }
            _status = false;
        }
        #endregion

        #region Emailing
        /// <summary>
        /// Запрашивает температуру городов пользователей у которых подтверждена учетная запись, при несоответствии рассылает прогнозы.
        /// </summary>
        public async void CheckAndEmailingAsync(object obj)
        {
            try
            {
                await _semaphoreSlim.WaitAsync();

                // Получаем список адресатов у которых подтверждена учетная запись
                List<Recipient> Recipients = await db.ApplicationUsers
                    .Where(a => a.EmailConfirmed)
                    .Select(a => new Recipient { City = a.City, Email = a.Email, Temp = a.PreviousTemp ?? 0 })
                    .ToListAsync();

                // Если количество адресатов меньше одного, то выходим из метода
                if (Recipients.Count < 1)
                {
                    return;
                }

                // Создаем список городов без повторений
                List<Cities.List> cities = new List<Cities.List>();
                foreach(Recipient recipient in Recipients)
                {
                    // Если город не был найден в списке, то добавляем его
                    if (!cities.Contains(recipient.City)) cities.Add(recipient.City);
                }

                // Создаем список температур в городах
                List<TempOfCity> tempOfCities = new List<TempOfCity>();
                foreach(Cities.List city in cities)
                {
                    // Если температура не была получена, то пропускаем данный город
                    Double? temp = await GetTempAsync(city);
                    if (!temp.HasValue)
                    {
                        continue;
                    }
                    tempOfCities.Add(new TempOfCity { City = city, Temp = temp.Value });
                }

                // Удаляем из списка адресаты у которых предыдущая температура в городе равна недавно полученной
                foreach(TempOfCity tempOfCity in tempOfCities)
                {
                    Recipients.RemoveAll(item => item.City == tempOfCity.City & item.Temp == tempOfCity.Temp);
                }
                // Если адресатов не осталось то выходим из метода
                if (Recipients.Count < 1)
                {
                    return;
                }

                // Иначе создаем словарь - Email адресата: температура в городе
                Dictionary<string, TempOfCity> RecepientsTempOfCity = new Dictionary<string, TempOfCity>();
                foreach(Recipient recipient in Recipients)
                {
                    // Если получатель каким-то образом уже содержится в словаре, то пропускаем его
                    if (RecepientsTempOfCity.ContainsKey(recipient.Email))
                    { 
                        continue;
                    }
                    // Иначе добавляем пару ключ: значение.
                    RecepientsTempOfCity.Add(recipient.Email, tempOfCities.Find(item => item.City == recipient.City));
                }
                // Если по какой-то причине словарь не содержит ни одной пары, то выходим из метода
                if (RecepientsTempOfCity.Count < 1)
                {
                    return;
                }

                // Иначе передаем словарь в метод рассылки сообщений адресатам
                await SendEmailToRecepientsAsync(RecepientsTempOfCity);
            }
            finally
            {
                _semaphoreSlim.Release();
            }
        }

        /// <summary>
        /// Рассылает списку адресатов прогноз температуры, соответствуя отслеживаемому 
        /// городу и изменяет его текущую температуру в учетной записи.
        /// </summary>
        /// <param name="recepientsTempOfCity">Словарь Email адресата: температура в городе.</param>
        private async Task SendEmailToRecepientsAsync(Dictionary<string, TempOfCity> recepientsTempOfCity)
        {
            using (SmtpClient client = new SmtpClient(App_GlobalResources.SMTP_Settings.ApplicationSMTP, Convert.ToInt32(App_GlobalResources.SMTP_Settings.AppicationSMPT_Port)))
            {
                // Настраиваем клиент
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                NetworkCredential networkCredential = new NetworkCredential(App_GlobalResources.SMTP_Settings.ApplicationEmail, App_GlobalResources.SMTP_Settings.ApplicationEmailPassword);
                client.EnableSsl = true;
                client.Credentials = networkCredential;
                foreach (KeyValuePair<string, TempOfCity> recepientTempOfCity in recepientsTempOfCity)
                {
                    // Получаем температуру и название города из значения словаря
                    string temp = recepientTempOfCity.Value.Temp.ToString();
                    string displayName = recepientTempOfCity.Value.City.GetType()
                        .GetMember(recepientTempOfCity.Value.City.ToString())
                        .SingleOrDefault()
                        .GetCustomAttribute<DisplayAttribute>()
                        .GetName();

                    // Если DisplayName не был получен, то присваиваем значение перечисления
                    if (String.IsNullOrEmpty(displayName))
                    {
                        displayName = recepientTempOfCity.Value.City.ToString();
                    }

                    // Формируем тело сообщения
                    string body = $"В городе {displayName} изменилась температура. Текущие данные: {temp}, градус Цельсия.";
                    body = $"<h4>{body}</h4>";

                    // Формируем и отправляем сообщениe
                    using (MailMessage email = new MailMessage(App_GlobalResources.SMTP_Settings.ApplicationEmail, recepientTempOfCity.Key))
                    {
                        email.IsBodyHtml = true;
                        email.Subject = "Прогноз температуры";
                        email.Body = body;
                        await client.SendMailAsync(email);
                    }

                    // Изменяем текущие данные о погоде в учетной записи адресата
                    ApplicationUser user = await db.ApplicationUsers.Where(a => a.Email == recepientTempOfCity.Key).SingleOrDefaultAsync();
                    db.ApplicationUsers.Attach(user);
                    user.PreviousTemp = recepientTempOfCity.Value.Temp;
                    await db.SaveChangesAsync();
                }
            }
        }
        #endregion

        #region API_Interaction
        /// <summary>
        /// Возвращает температуру города, определенного перечислением или null, если температура не была получена.
        /// </summary>
        /// <param name="city">Город, температуру которого необходимо вернуть.</param>
        private async Task<Double?> GetTempAsync(Cities.List city)
        {
            if (!Enum.IsDefined(typeof(Cities.List), city))
            {
                return null;
            }

            string cityString = city.ToString();

            string uri1 = $"https://api.weatherbit.io/v2.0/current?city={cityString}&key={APIKEY1}";
            string uri2 = $"http://api.worldweatheronline.com/premium/v1/weather.ashx?key={APIKEY2}&q={cityString}&tp=1&format=json"; // Более точный

            // Получаем результат запроса из WeatherBit
            string jsonString = await GetJsonStringAsync(uri1);

            // Если полученный json равен null, то запрашиваем прогноз из другого сервиса
            if (!String.IsNullOrEmpty(jsonString))
            {
                dynamic json = JsonConvert.DeserializeObject(jsonString);
                return (Double?)json.data[0].temp;
            }
            else
            {
                // Получаем резульат запроса из worldweatheronline
                jsonString = await GetJsonStringAsync(uri2);

                // Если полученный json равен null возвращаем null
                if (String.IsNullOrEmpty(jsonString))
                {
                    return null;
                }

                dynamic json = JsonConvert.DeserializeObject(jsonString);
                return (Double?)json.data.current_condition[0].temp_C;
            }
        }

        /// <summary>
        /// Возвращает JsonString из Get запроса, заданного параметром uri, при ошибке вернется null.
        /// </summary>
        /// <param name="uri">Строка запроса.</param>
        /// <returns>Json результат, если запрос успешен, иначе null.</returns>
        private async Task<string> GetJsonStringAsync(string uri)
        {
            // Попытка получить json
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

                using (HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    return await reader.ReadToEndAsync();
                }
            }
            // При исключении вернется null
            catch
            {
                return null;
            }
        }
        #endregion
    }
}