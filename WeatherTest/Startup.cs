﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WeatherTest.Startup))]
namespace WeatherTest
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
